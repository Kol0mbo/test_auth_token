from rest_framework import  views, response, status
from user.models import Profile
from user.serializer import ProfileSerializer, UserSerializer
class ProfileView(views.APIView):

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get(self, request):
        profile = Profile.objects.all()
        return response.Response(ProfileSerializer(profile, many=True).data)
    
    def post(self, request):
        # Create User
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        # Create Profile
        serializer = ProfileSerializer(data=request.data, context={'user': user})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return response.Response(serializer.data, status=status.HTTP_201_CREATED)




