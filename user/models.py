from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)

    def __str__(self) -> str:
        return "%s %s" % (self.first_name, self.last_name)