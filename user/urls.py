from django.urls import path
from user.views import ProfileView

urlpatterns = [
    path('profile/', ProfileView.as_view()),
]
