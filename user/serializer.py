from rest_framework import serializers
from django.contrib.auth.models import User
from user.models import Profile

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', )

    def create(self, validated_data):
        instance = User(
            username=validated_data.get('username'),
            email=validated_data.get('email'),
            is_active=True,
        )
        instance.set_password(validated_data.get('password'))
        instance.save()
        return instance

class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only = True)
    password = serializers.CharField(write_only = True)
    email = serializers.CharField(write_only = True)
    username = serializers.CharField(write_only = True)

    class Meta:
        model = Profile
        fields = ('user', 'first_name', 'last_name', 'password', 'email', 'username',)

    def create(self, validated_data):
        instance = Profile(
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
        )
        instance.user = self.context.get('user')
        instance.save()
        return instance

       
