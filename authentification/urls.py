from django.urls import path
from authentification.views import AuthToken

urlpatterns = [
  path('api-token-auth/', AuthToken.as_view()),
]
