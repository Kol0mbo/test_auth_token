from django.contrib import admin
from django.urls.conf import include
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('authentification.urls')),
    path('user/', include('user.urls')),
]
